
# Itsa Web Service
Sencilla aplicación web para administrar materias, reticulas y horarios, desarrollada en Spring boot para el ITSA.

[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy?template=https://github.com/heroku/node-js-sample)

# Documentación rest api
Por ahora solo se cuenta con operaciones `get` en los horarios. Para consultar los horarios en formato json.

	   http://localhost:8080/api/v1/schedule
|parametros soportados| valores permitidos | valor por defecto |
|--|--|--|
|career|1 - 8|1|
|semester|1 - 8|1|
|turn|"morning" o "evening"|"morning"|

**Ejemplo**
	
	http://localhost:8080/api/v1/schedule?semester=1&career=7

El ejemplo anterior muestra el horario de la carrera de Ing. informática de primer semestre turno matutino.

  
```json
{
    "id": 2,
    "career": {
        "id": 7,
        "name": "Ing. Informática"
    },
    "semester": 1,
    "turn": "morning",
    "uuid": "2ba89d82-9f01-4b9c-bb1b-77334e9bf595",
    "year": "2018-06-13T14:25:38.000+0000",
    "week": [
	    {
            "day": 0, // Lunes
            "uuid": "ff6e92d5-f2fa-45e5-9aca-b23e26fe78fd",
			// Cada dia tiene una lista de materias
            "subjects": [{
                    "id": 31,
                    "startHour": "07:00 a.m.",
                    "finishHour": "08:00 a.m.",
                    "uuid": "186914c7-540e-4ed9-8a0b-c6e8931d4249",
                    "subject": {
                        "id": 12,
                        "name": "Taller de ética",
                        "key": "ACA-0907",
                        "ht": 0,
                        "hp": 4,
                        "credits": 4,
                        "color": "#795548" }
	                },
	                {...},
	                {...}
	            ]
	    },
        {...}, // martes
        {...}, // miercoles
        {...}, // jueves
        {...} // viernes
    ]
}
```

**Carreras**

| Id de referencia| Carrera |
|--|--|
|1|Contador Público|
|2|Ing. Bioquimíca|
|3|Ing. Civil|
|4|Ing. en Gestión Empresarial|
|5|Ing. en Innov. Agr. Sust.|
|6|Ing. Industrial|
|7|Ing. Informática|
|8|Ing. Sistemas Comp.|

### Todos

- Vista web(Spring MVC) para consultar y editar horario.
- Modificar la generacion de colors en las materias.
- Utiliza postgreSQL en vez de MySQL de oracle.
- Cambiar a un mejor css framework; bulma css muestra bugs en unas tablas.
- Extender la api para realizar mas chucherias.
- Implementar JSON Web Token (JWT) en la API.

License
----

The MIT License (MIT)

Copyright (c) 2018 Jose Carabez

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
