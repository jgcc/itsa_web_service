package com.itsa.itsa_ws;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ItsaWsApplicationTests {

	 @Test
	 public void greeterSaysHello() {
		 assertThat("", containsString(""));
	 }
}
