package com.itsa.itsa_ws.controllers;

import com.itsa.itsa_ws.models.Career;
import com.itsa.itsa_ws.models.Reticle;
import com.itsa.itsa_ws.models.Subject;
import com.itsa.itsa_ws.repositories.CareerRepository;
import com.itsa.itsa_ws.repositories.ReticleRepository;
import com.itsa.itsa_ws.repositories.SubjectRepository;
import com.itsa.itsa_ws.repositories.SubjectSpecs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Controller
public class ReticleController {

    @Autowired
    private ReticleRepository reticleRepository;

    @Autowired
    private CareerRepository careerRepository;

    @Autowired
    private SubjectRepository subjectRepository;


    // < IdDelaMateria, listaDeClaves >
    final Map<Long, String[]> mapKeys;

    public ReticleController() {
        mapKeys = new HashMap<>();
        mapKeys.put(5L, new String[]{""});
        mapKeys.put(1L, new String[]{"cpm", "cpc", "cpd", "acg", "cpd", "cpf", "cpj", "cpo", "cph", "fid", "cpa"});
        mapKeys.put(2L, new String[]{"bqg", "aej", "bqp", "bqw", "aeo", "bqf", "bqq", "tig", "bqj", "aem", "bqc", "ald"});
        mapKeys.put(3L, new String[]{"icc", "ica", "icm", "acad", "ict", "aec", "aco", "icf", "acf", "icg", "ice", "icj", "ici", "icd", "coc", "aco"});
        mapKeys.put(4L, new String[]{"gec", "gec", "gef", "aeb", "ged", "aec", "gee", "acg", "geg", "tig", "aed", "aeg", "dnh", "dnm", "dnq", "dnd", "aeb"});
        mapKeys.put(6L, new String[]{"inh", "inc", "inn", "aec", "inq", "inj", "inf", "inr", "aed", "inh"});
        mapKeys.put(7L, new String[]{"aec", "icf", "ife", "ifd", "aeb", "aed", "iff", "ifc", "aeh", "ifm", "ifr", "ifh"});
        mapKeys.put(8L, new String[]{"aed", "sch", "aec", "acg", "scc", "scf", "scd", "sca", "tig", "scb", "dwt", "scg", "tps", "pmt", "aeb"});
    }

    @GetMapping("/reticle")
    public String list(@RequestParam(value = "career", defaultValue = "1", required = false) Long careerId, Model model) {
        List<Reticle> list = reticleRepository.findByCareerId(careerId);
        model.addAttribute("reticleList", list);
        model.addAttribute("careerList", careerRepository.findAll());
        return "reticle/list";
    }

    @GetMapping(value = "/api/v1/reticle", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<Reticle> list(@RequestParam(value = "career", defaultValue = "1", required = false) Long careerId) {
        return reticleRepository.findByCareerId(careerId);
    }

    @GetMapping("/reticle/save")
    public String add(@RequestParam(value = "id", defaultValue = "1", required = false) Long id, Model model) {
        Optional<Career> career = careerRepository.findById(id);
        long careerCount = careerRepository.count();
        if (id > careerCount)
            return "redirect:/reticle";

        List<Subject> arrayList = subjectRepository.findAll(SubjectSpecs.withSubjecKeys(mapKeys.get(id)));

        career.ifPresent(c -> {
            model.addAttribute("careerList", careerRepository.findAll());
            model.addAttribute("subjectList", arrayList);
            model.addAttribute("reticle", new Reticle(c));
        });
        return "reticle/save";
    }

    @PostMapping(value = "/reticle/save")
    public String save(@ModelAttribute Reticle reticle,
                       @RequestParam(value = "subjectList[]") List<Long> subjectList) {
        Optional<Career> optionalcareer = careerRepository.findById(reticle.getCareer().getId());
        optionalcareer.ifPresent(career -> {
            reticle.getCareer().setName(career.getName());
            subjectRepository.findAllById(subjectList).forEach(reticle::addSubject);
            reticleRepository.save(reticle);
        });
        return "redirect:/reticle/save?id="+reticle.getCareer().getId();
    }

    @GetMapping(value = "/export", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<Reticle> export() {
        return reticleRepository.findAll();
    }
}
