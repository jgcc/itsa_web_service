package com.itsa.itsa_ws.controllers;

import com.itsa.itsa_ws.models.Subject;
import com.itsa.itsa_ws.repositories.SubjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.Optional;

@Controller
public class SubjectsController {

    @Autowired
    private SubjectRepository repository;

    private final String colors[] = {
        "#FF9800",
        "#3F51B5",
        "#F44336",
        "#607D8B",
        "#2196F3",
        "#009688",
        "#009688",
        "#E91E63",
        "#9C27B0",
        "#FFEB3B",
        "#4CAF50",
        "#795548",
        "#00BCD4",
        "#CDDC39",
        "#9E9E9E"
    };
    private byte index;

    @GetMapping("/subject")
    public String list(Model model) {
        model.addAttribute("subject", new Subject());
        model.addAttribute("subjectList", repository.findAllByOrderByKeyAsc());
        return "subjects/list";
    }

    @PostMapping("/subject")
    public String add(@Valid @ModelAttribute("subject") Subject subject, BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("subjectList", repository.findAllByOrderByKeyAsc());
            return "subjects/list";
        }

        if (index >= colors.length)
            index = 0;

        if (subject.getColor().isEmpty()) {
            subject.setColor(colors[index]);
        }

        subject.setCredits(subject.getHp() + subject.getHt());
        repository.save(subject);

        index++;

        return "redirect:/subject";
    }

    @GetMapping("subject/edit/{subjectId}")
    public String edit(@PathVariable(value = "subjectId") Long id, Model model) {
        Optional<Subject> subject = repository.findById(id);
        subject.ifPresent(s -> model.addAttribute("subject", s));
        return "subjects/edit";
    }

    @GetMapping("subject/delete/{subjectId}")
    public String delete(@PathVariable(value = "subjectId") Long id, Model model) {
        repository.deleteById(id);
        return "redirect:/";
    }
}
