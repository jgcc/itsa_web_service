package com.itsa.itsa_ws.controllers;

import com.itsa.itsa_ws.models.*;
import com.itsa.itsa_ws.repositories.CareerRepository;
import com.itsa.itsa_ws.repositories.ReticleRepository;
import com.itsa.itsa_ws.repositories.ScheduleRepository;
import com.itsa.itsa_ws.repositories.SubjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
public class ScheduleController {

    private static final int MAX_HOURS_PER_DAY = 6;

    @Autowired
    private CareerRepository careerRepository;

    @Autowired
    private ReticleRepository reticleRepository;

    @Autowired
    private ScheduleRepository scheduleRepository;

    @Autowired
    private SubjectRepository subjectRepository;

    @GetMapping(value = {"/", "/schedule"})
    public String list(@RequestParam(value = "career", defaultValue = "1", required = false) Long careerId,  Model model) {
        model.addAttribute("careerList", careerRepository.findAllByOrderByName());
        return "schedule/list";
    }

    @GetMapping(value = "/api/v1/schedule", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody Schedule show(@RequestParam(value = "career", defaultValue = "1", required = false) Long careerId,
                  @RequestParam(value = "semester", defaultValue = "1", required = false) int semester,
                  @RequestParam(value = "turn", defaultValue = "morning", required = false) Schedule.Turn turn) {

        final Schedule[] responseSchedule = new Schedule[1];

        Optional<Career> optionalcareer = careerRepository.findById(careerId);
        optionalcareer.ifPresent(career -> {
            Optional<Schedule> optionalSchedule = scheduleRepository.findByCareerAndSemesterAndTurn(career, semester, turn);
            responseSchedule[0] = optionalSchedule.orElseGet(Schedule::new);
        });

        return responseSchedule[0];
    }

    @GetMapping("/schedule/save")
    public String save(@RequestParam(value = "career", defaultValue = "1", required = false) Long careerId,
                       @RequestParam(value = "semester", defaultValue = "1", required = false) int semester,
                       @RequestParam(value = "turn", defaultValue = "morning", required = false) Schedule.Turn turn,
                       Model model) {

        Calendar startHour = Calendar.getInstance();
        Calendar finishHour = Calendar.getInstance();
        DateFormat formatter = new SimpleDateFormat("hh:mm a", Locale.getDefault());
        startHour.set(Calendar.MINUTE, 0);
        finishHour.set(Calendar.MINUTE, 0);

        if (turn == Schedule.Turn.morning) {
            startHour.set(Calendar.HOUR_OF_DAY, 6);
            finishHour.set(Calendar.HOUR_OF_DAY, 7);
        } else {
            startHour.set(Calendar.HOUR_OF_DAY, 12);
            finishHour.set(Calendar.HOUR_OF_DAY, 13);
        }

        String hourList[] = new String[MAX_HOURS_PER_DAY];
        for (int i = 0; i < hourList.length; i++) {
            startHour.add(Calendar.HOUR_OF_DAY, 1);
            finishHour.add(Calendar.HOUR_OF_DAY, 1);
            hourList[i] = formatter.format(startHour.getTime()) +" - "+ formatter.format(finishHour.getTime());
        }

        Optional<Reticle> optionalReticle = reticleRepository.findByCareerId(careerId)
                .stream()
                .filter(reticle -> reticle.getSemester() == semester)
                .findFirst();

        optionalReticle.ifPresent(reticle -> {
            Optional<Subject> optionalFreeHour = subjectRepository.findById(301L); // Busca la hora libre para todas las carreras
            optionalFreeHour.ifPresent(subject -> {
                reticle.getSubjects().add(0, subject);
                model.addAttribute("subjectList", reticle.getSubjects());
            });
        });
        model.addAttribute("hourList", hourList);
        model.addAttribute("careerList", careerRepository.findAllByOrderByName());

        return "schedule/save";
    }

    @PostMapping("/schedule/save")
    public String save(HttpServletRequest request,
                       @RequestParam("career") Long careerId,
                       @RequestParam("semester") int semester,
                       @RequestParam("turn") Schedule.Turn turn,
                       Model model) {
        Optional<Career> optionalcareer = careerRepository.findById(careerId);
        optionalcareer.ifPresent(career -> {
            Schedule schedule = new Schedule();
            schedule.setYear(Calendar.getInstance().getTime());
            schedule.setTurn(turn);
            schedule.setSemester(semester);
            schedule.setCareer(career);


            Map<String, String[]> parameterMap = request.getParameterMap();

            List<DayOfWeek> week = new ArrayList<>();
            for (int day = 0; day < 5; day++) {
                DayOfWeek dayOfWeek = new DayOfWeek();
                dayOfWeek.setDay(day);
                List<ScheduleDay> scheduleDayList = new ArrayList<>();
                int finalDay = day;
                parameterMap.forEach((hour, subjectList) -> {
                    if (hour.contains(":")) {
                        Optional<Subject> optionalSubject = subjectRepository.findById(Long.parseLong(subjectList[finalDay]));
                        optionalSubject.ifPresent(subject -> {
                            ScheduleDay scheduleDay = new ScheduleDay();
                            String hourArray[] = hour.split(" - ");
                            scheduleDay.setStartHour(hourArray[0]);
                            scheduleDay.setFinishHour(hourArray[1]);
                            scheduleDay.setSubject(subject);
                            scheduleDay.setDayOfWeek(dayOfWeek);
                            scheduleDayList.add(scheduleDay);
                        });
                    }
                });
                dayOfWeek.setSubjects(scheduleDayList);
                dayOfWeek.setSchedule(schedule);
                week.add(dayOfWeek);
            }
            schedule.setWeek(week);
            scheduleRepository.save(schedule);
        });
        return "redirect:/schedule";
    }

}
