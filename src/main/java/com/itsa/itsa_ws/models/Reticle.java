package com.itsa.itsa_ws.models;

import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Table(name = "t_reticles")
@Entity
@Where(clause = "deleted=0")
public class Reticle {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private int semester;

    @OneToOne
    private Career career;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(
            name = "t_reticle_subject",
            joinColumns = @JoinColumn(name = "reticle_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "subject_id", referencedColumnName = "id"))
    @OrderBy("name")
    private List<Subject> subjects;

    @Column
    private Boolean deleted;

    public Reticle() {
        this.id = null;
        this.career = null;
        this.subjects = new ArrayList<>();
        this.semester = 1;
        deleted = false;
    }

    public Reticle(Career career) {
        this.id = null;
        this.career = career;
        this.subjects = new ArrayList<>();
        this.semester = 1;
        deleted = false;
    }

    public Reticle(Long id, Career career) {
        this.id = id;
        this.career = career;
        this.subjects = new ArrayList<>();
        this.semester = 1;
        deleted = false;
    }

    public Long getId() {
        return id;
    }

    public Career getCareer() {
        return career;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setCareer(Career career) {
        this.career = career;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }

    public void addSubject(Subject subject) {
        this.subjects.add(subject);
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getSemester() {
        return semester;
    }

    public void setSemester(int semester) {
        this.semester = semester;
    }
}
