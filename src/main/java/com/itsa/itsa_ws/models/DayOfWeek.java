package com.itsa.itsa_ws.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Table(name = "t_dayofweek")
@Entity
public class DayOfWeek {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @Column
    private int day;

    @Column
    private String uuid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "schedule_id", nullable = false)
    @JsonIgnore
    private Schedule schedule;

    @OneToMany(mappedBy = "dayOfWeek",
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private List<ScheduleDay> subjects;

    public DayOfWeek() {
        day = 0;
        uuid = UUID.randomUUID().toString();
    }

    public DayOfWeek(int day) {
        this.day = day;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public List<ScheduleDay> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<ScheduleDay> subjects) {
        this.subjects = subjects;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
