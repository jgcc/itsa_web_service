package com.itsa.itsa_ws.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.*;

@Table(name = "t_schedule")
@Entity
@Where(clause = "deleted=0")
public class Schedule {

    public enum Turn { morning, evening }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    private Career career;

    @Column
    private int semester;

    @Column
    @Enumerated(EnumType.STRING)
    private Turn turn;

    @Column
    private String uuid;

    @Column
    private Date year;

    @OneToMany(mappedBy = "schedule",
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private List<DayOfWeek> week;

    @Column
    @JsonIgnore
    private Boolean deleted;

    public Schedule() {
        deleted = false;
        week = Collections.EMPTY_LIST;
        uuid = UUID.randomUUID().toString();
    }

    public Schedule(Career career, int semester, Turn turn, Date year, List<DayOfWeek> week) {
        this.career = career;
        this.semester = semester;
        this.turn = turn;
        this.year = year;
        this.week = week;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Career getCareer() {
        return career;
    }

    public void setCareer(Career career) {
        this.career = career;
    }

    public int getSemester() {
        return semester;
    }

    public void setSemester(int semester) {
        this.semester = semester;
    }

    public Turn getTurn() {
        return turn;
    }

    public void setTurn(Turn turn) {
        this.turn = turn;
    }

    public Date getYear() {
        return year;
    }

    public void setYear(Date year) {
        this.year = year;
    }

    public List<DayOfWeek> getWeek() {
        return week;
    }

    public void setWeek(List<DayOfWeek> week) {
        this.week = week;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
