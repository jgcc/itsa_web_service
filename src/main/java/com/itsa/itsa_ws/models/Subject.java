package com.itsa.itsa_ws.models;


import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Table(name = "t_subjects")
@Entity
@Where(clause = "deleted=0")
public class Subject {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty(message = "El nombre no puede estar vacio.")
    @Column(nullable = false)
    private String name;

    @NotEmpty(message = "La clave no puede estar vacia.")
    @Column(nullable = false, name = "subject_key")
    private String key;

    @Column(nullable = false)
    private int ht;

    @Column(nullable = false)
    private int hp;

    @Column(nullable = false)
    private int credits;

    @Column(nullable = false)
    private String color;

    @Column
    private Boolean deleted;

    public Subject(String name, String key, int ht, int hp, int credits, String color) {
        this.id = null;
        this.name = name;
        this.key = key;
        this.ht = ht;
        this.hp = hp;
        this.credits = credits;
        this.color = color;
        deleted = false;
    }

    public Subject() {
        this.id = null;
        this.name = "";
        this.key = "";
        this.ht = 0;
        this.hp = 0;
        this.credits = 0;
        this.color = "";
        deleted = false;
    }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getName() { return name; }

    public void setName(String name) {
        this.name = name.substring(0,1).toUpperCase() + name.substring(1);
    }

    public String getKey() {
        return key;
    }

    public int getHt() {
        return ht;
    }

    public int getHp() {
        return hp;
    }

    public int getCredits() {
        return credits;
    }

    public String getColor() {
        return color;
    }

    public void setKey(String key) {
        this.key = key.toUpperCase();
    }

    public void setHt(int ht) {
        this.ht = ht;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setCredits(int credits) {
        this.credits = credits;
    }
}
