package com.itsa.itsa_ws.models;

import javax.persistence.*;

@Entity
@Table(name = "t_careers")
public class Career {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column
    private Boolean deleted;

    public Career() {
        id = null;
        name = null;
        deleted = false;
    }

    public Career(String name) {
        id = null;
        this.name = name;
        deleted = false;
    }

    public Career(Long id, String name) {
        this.id = id;
        this.name = name;
        deleted = false;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
