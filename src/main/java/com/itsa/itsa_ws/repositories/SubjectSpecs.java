package com.itsa.itsa_ws.repositories;

import com.itsa.itsa_ws.models.Subject;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class SubjectSpecs {

    public static Specification<Subject> withSubjecKeys(String... keys) {
        return new Specification<Subject>() {
            @Override
            public Predicate toPredicate(Root<Subject> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicateList = new ArrayList<>();

                // Materias de tronco común
                predicateList.add(criteriaBuilder.or(
                        criteriaBuilder.like(root.get("key"), "%aca%"),
                        criteriaBuilder.like(root.get("key"), "%acc%"),
                        criteriaBuilder.like(root.get("key"), "%acd%"),
                        criteriaBuilder.like(root.get("key"), "%acf%"),
                        criteriaBuilder.like(root.get("key"), "%acg%"),
                        criteriaBuilder.like(root.get("key"), "%aef%"),
                        criteriaBuilder.like(root.get("key"), "%ssk%"),
                        criteriaBuilder.like(root.get("key"), "%rpk%"),
                        criteriaBuilder.like(root.get("key"), "%tug%")
                ));

                for (String key : keys) {
                    predicateList.add(
                            criteriaBuilder.or(
                                    criteriaBuilder.like(root.get("key"), "%" + key + "%"))
                    );
                }

                criteriaQuery.orderBy(criteriaBuilder.asc(root.get("name")));
                return criteriaBuilder.or(predicateList.toArray(new Predicate[predicateList.size()]));
            }
        };
    }

}
