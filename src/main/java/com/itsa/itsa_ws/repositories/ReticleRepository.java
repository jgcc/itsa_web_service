package com.itsa.itsa_ws.repositories;

import com.itsa.itsa_ws.models.Reticle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ReticleRepository extends JpaRepository<Reticle, Long> {
    List<Reticle> findByCareerId(Long id);
}
