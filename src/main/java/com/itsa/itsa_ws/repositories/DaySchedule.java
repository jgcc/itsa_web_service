package com.itsa.itsa_ws.repositories;

import com.itsa.itsa_ws.models.ScheduleDay;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DaySchedule extends JpaRepository<ScheduleDay, Long> {
}
