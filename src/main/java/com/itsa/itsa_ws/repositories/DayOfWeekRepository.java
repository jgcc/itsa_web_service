package com.itsa.itsa_ws.repositories;

import com.itsa.itsa_ws.models.DayOfWeek;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DayOfWeekRepository extends JpaRepository<DayOfWeek, Long> {
}
