package com.itsa.itsa_ws.repositories;

import com.itsa.itsa_ws.models.Career;
import com.itsa.itsa_ws.models.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ScheduleRepository extends JpaRepository<Schedule, Long> {
    Optional<Schedule> findByCareerAndSemesterAndTurn(Career careerId, int semester, Schedule.Turn turn);
}
