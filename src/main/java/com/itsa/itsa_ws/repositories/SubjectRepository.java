package com.itsa.itsa_ws.repositories;

import com.itsa.itsa_ws.models.Subject;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubjectRepository extends CrudRepository<Subject, Long>, JpaSpecificationExecutor {
    List<Subject> findAllByOrderByKeyAsc();
}
