package com.itsa.itsa_ws.repositories;

import com.itsa.itsa_ws.models.Career;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CareerRepository extends CrudRepository<Career, Long> {
    List<Career> findAllByOrderByName();
}
