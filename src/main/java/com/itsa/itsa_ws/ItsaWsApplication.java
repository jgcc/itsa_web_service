package com.itsa.itsa_ws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ItsaWsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ItsaWsApplication.class, args);
	}

}
